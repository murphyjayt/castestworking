﻿using System;
using System.Web.Security;

// added this resource

namespace CASExample2
{
    /*
     added this so MyRoleProvider will inherit from RoleProvider
     be sure to right click on MyRoleProvider --> Quick Actions and Refactoring --> select Implement Abstract Class
     Made changes to:
        ApplicationName to fix syntax errors in the get & set methods (=> vs {})
        mehtod GetRolesForUser
    */

    public class MyRoleProvider : RoleProvider
    {
        public override string ApplicationName
        {
            /* change these 2 lines to what follows;
             it doesn't like the => on the get and set, so changed to use {} around both lines
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
            */
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        /* we will make changes to GetRolesForUser method */

        public override string[] GetRolesForUser(string username)
        {
            if (username == "mybamausername" || username == "othermybamausername") //This tells the little role manager that these 2 mybamausernames would be admins.
            {
                return new[] { "admin" };
            }

            return new[] { "" };
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}