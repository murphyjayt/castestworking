﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  https://go.microsoft.com/fwlink/?LinkId=301880
  -->
<configuration>
  <configSections>
    <section name="casClientConfig" type="DotNetCasClient.Configuration.CasClientConfiguration, DotNetCasClient" />
  </configSections>
  <appSettings>
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <!-- appSettings configuation from VCU's example:
      https://ts.vcu.edu/askit/web-tools/cas-central-authentication-service/integrating-cas-with-a-web-application/
    -->
    <add key="casLoginURL" value="https://login.ua.edu/cas/login" />
    <add key="casValidateURL" value="https://login.ua.edu/cas/samlValidate" />
    <!-- Not sure if we need serviceURL; how is this different than the serverName in casClientConfig? -->
    <!--<add key="serviceURL" value="URL to the main page of your application" />-->
    <!-- end of VCU example adding appSettings keys -->
  </appSettings>

  <system.web>
    <compilation debug="true" targetFramework="4.6.1" />
    <httpRuntime targetFramework="4.6.1" />
    <httpModules>
      <add name="ApplicationInsightsWebTracking" type="Microsoft.ApplicationInsights.Web.ApplicationInsightsHttpModule, Microsoft.AI.Web" />
      <add name="DotNetCasClient" type="DotNetCasClient.CasAuthenticationModule,DotNetCasClient" />
    </httpModules>
    <authentication mode="Forms">
      <forms name=".DotNetCasClientAuth" loginUrl="https://login.ua.edu/cas/login" cookieless="UseCookies" />
      <!-- old copy before making UA changes <forms name=".DotNetCasClientAuth" loginUrl="https://cas.example.com/cas/login" cookieless="UseCookies" />-->
    </authentication>
    <!--
    added this section "roleManager"  inside of system.web
    This will handle checking user roles
    MyRoleProvider is a class we will create
     -->
    <roleManager enabled="true" defaultProvider="MyRoleProvider">
      <providers>
        <add name="MyRoleProvider" type="CASExample2.MyRoleProvider" />
      </providers>
    </roleManager>
    <!-- end of roleManager section-->
  </system.web>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" culture="neutral" publicKeyToken="30ad4fe6b2a6aeed" />
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Optimization" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="1.1.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-1.5.2.14234" newVersion="1.5.2.14234" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <modules>
      <remove name="ApplicationInsightsWebTracking" />
      <remove name="DotNetCasClient" />
      <add name="ApplicationInsightsWebTracking" type="Microsoft.ApplicationInsights.Web.ApplicationInsightsHttpModule, Microsoft.AI.Web" preCondition="managedHandler" />
      <add name="DotNetCasClient" type="DotNetCasClient.CasAuthenticationModule,DotNetCasClient" />
    </modules>
  </system.webServer>
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.7.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:default /nowarn:1659;1699;1701" />
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.7.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:default /nowarn:41008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+" />
    </compilers>
  </system.codedom>
  <!--
        *======================
        * Core configuration
        * REQUIRED
        *======================
        Required Attributes:

        - casServerLoginUrl
                URL of CAS login form.
        - serverName
                Host name of the server hosting this application.  This is used to generate URLs
                that will be sent to the CAS server for redirection.  The CAS server must be able
                to resolve this host name.  If your web application is behind a load balancer, SSL
                offloader, or any other type of device that accepts incoming requests on behalf of
                the web application, you will generally need to supply the public facing host name
                unless your CAS server is in the same private network as the application server.
                The protocol prefix is optional (http:// or https://).  If you are using a non-
                standard port number, be sure to include it (i.e., server.school.edu:8443 or
                https://cas.example.com:8443).  Do not include the trailing backslash.
        - casServerUrlPrefix
                URL to root of CAS server application.
        - ticketValidatorName
                Supported values: Cas10, Cas20, and Saml11.
                Name of ticket validator that validates CAS tickets using a particular protocol.

        Optional Attributes:
        - gateway (default false)
                Enable CAS gateway feature, see http://www.jasig.org/cas/protocol section 2.1.1.
        - renew (default true)
                Force user to reauthenticate to CAS before accessing this application.
                This provides additional security at the cost of usability since it effectively
                disables SSO for this application.
        - singleSignOut (default true)
                Enables this application to receive CAS single sign-out messages sent when the
                user's SSO session ends.  This will cause the user's session in this application
                to be destroyed.
        - ticketTimeTolerance
                SAML ticket validator property to allow at most the given time difference in ms
                between artifact (ticket) timestamp and CAS server system time.  Increasing this
                may have negative security consequences; we recommend fixing sources of clock drift
                rather than increasing this value.
        - notAuthorizedUrl
                The URL to redirect to when the request has a valid CAS ticket but the user is
                not authorized to access the URL or resource.  If this option is set, users will
                be redirected to this URL.  If it is not set, the user will be redirected to the
                CAS login screen with a Renew option in the URL (to force for alternate credential
                collection).
        - serviceTicketManager
                Supported values: CacheTicketManager.
                The service ticket manager to use to store tickets returned by the CAS server for
                validation, revocation, and single sign out support.
                Omit this attribute to disable state management based on stored CAS assertion.
        - proxyTicketManager
                Supported values: CacheProxyTicketManager.
                The proxy ticket manager to use to maintain state during proxy ticket requests.
                Omit this attribute to disable proxy support.
        - gatewayStatusCookieName (default "cas_gateway_status")
                The name of the cookie used to store the Gateway status (NotAttempted, Success, Failed).
                This cookie is used to prevent the client from attempting to gateway authenticate every request.
        - cookiesRequiredUrl
                The URL to redirect to when the client is not accepting session cookies.  This
                condition is detected only when gateway is enabled.  This will lock the users onto
                a specific page.  Otherwise, every request will cause a silent round-trip to the
                CAS server, adding a parameter to the URL.
        - gatewayParameterName (default "gatewayResponse")
                The URL parameter to append to outbound CAS request's ServiceName when initiating
                an automatic CAS Gateway request.  This parameter plays a role in detecting whether
                or not the client has cookies enabled.
                Define this attribute only if the default parameter name has a meaning elsewhere
                in your application.
    -->
  <!--
  might need to change ticketValidatorName from Cas20 to Saml11
          ticketValidatorName="Saml11"
  also should proxyTicketManager="" or just be omitted?
          - proxyTicketManager
                Supported values: CacheProxyTicketManager.
                The proxy ticket manager to use to maintain state during proxy ticket requests.
                Omit this attribute to disable proxy support.
  -->
  <casClientConfig
    casServerLoginUrl="https://login.ua.edu/cas/login"
    casServerUrlPrefix="https://login.ua.edu/cas/"
    serverName="https://apps.ccs.ua.edu/"
    notAuthorizedUrl="~/Public/NotAuthorized"
    cookiesRequiredUrl="~/CookiesRequired.aspx"
    redirectAfterValidation="true"
    gateway="false"
    renew="false"
    singleSignOut="true"
    ticketTimeTolerance="5000"
    ticketValidatorName="Saml11"
    proxyTicketManager=""
    serviceTicketManager="CacheServiceTicketManager"
    gatewayStatusCookieName="CasGatewayStatus" />
  <system.diagnostics>
    <trace autoflush="true" useGlobalLock="false" />
    <sharedListeners>
      <!--
      Writing trace output to a log file is recommended.
      IMPORTANT:
      The user account under which the containing application pool runs
      must have privileges to create and modify the trace log file.
    -->
      <add name="TraceFile"
           type="System.Diagnostics.TextWriterTraceListener"
           initializeData="C:\inetpub\logs\LogFiles\DotNetCasClient-CASExample2.Log"
           traceOutputOptions="DateTime" />
    </sharedListeners>
    <sources>
      <!-- Provides diagnostic information on module configuration parameters. -->
      <source name="DotNetCasClient.Config" switchName="Config" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="TraceFile" />
        </listeners>
      </source>
      <!-- Traces IHttpModule lifecycle events and meaningful operations performed therein. -->
      <source name="DotNetCasClient.HttpModule" switchName="HttpModule" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="TraceFile" />
        </listeners>
      </source>
      <!-- Provides protocol message and routing information. -->
      <source name="DotNetCasClient.Protocol" switchName="Protocol" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="TraceFile" />
        </listeners>
      </source>
      <!-- Provides details on security operations and notable security conditions. -->
      <source name="DotNetCasClient.Security" switchName="Security" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="TraceFile" />
        </listeners>
      </source>
    </sources>
    <switches>
      <!--
      Set trace switches to appropriate logging level.  Recommended values in order of increasing verbosity:
       - Off
       - Error
       - Warning
       - Information
       - Verbose
    -->
      <!--
      Config category displays detailed information about CasAuthenticationModule configuration.
      The output of this category is only displayed when the module is initialized, which happens
      for the first request following application/server startup.
    -->
      <add name="Config" value="Verbose" />
      <!--
      Set this category to Verbose to trace HttpModule lifecycle events in CasAuthenticationModule.
      This category produces voluminous output in Verbose mode and should be avoided except for
      limited periods of time troubleshooting vexing integration problems.
    -->
      <add name="HttpModule" value="Information" />
      <!--
      Set to Verbose to display protocol messages between the client and server.
      This category is very helpful for troubleshooting integration problems.
    -->
      <add name="Protocol" value="Verbose" />
      <!--
      Displays important security-related information.
    -->
      <add name="Security" value="Information" />
    </switches>
  </system.diagnostics>
</configuration>
<!-- old copy before making UA changes -->
<!--
<casClientConfig
  casServerLoginUrl="https://cas.example.com/cas/login"
  casServerUrlPrefix="https://cas.example.com/cas/"
  serverName="cas.example.com"
  notAuthorizedUrl="~/NotAuthorized.aspx"
  cookiesRequiredUrl="~/CookiesRequired.aspx"
  redirectAfterValidation="true"
  gateway="false"
  renew="false"
  singleSignOut="true"
  ticketTimeTolerance="5000"
  ticketValidatorName="Cas20"
  proxyTicketManager="CacheProxyTicketManager"
  serviceTicketManager="CacheServiceTicketManager"
  gatewayStatusCookieName="CasGatewayStatus" />
</configuration>
-->