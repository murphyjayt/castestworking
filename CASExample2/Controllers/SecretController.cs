﻿using DotNetCasClient;
using DotNetCasClient.Security;
using System.Web.Mvc;

// added this resource

namespace CASExample2.Controllers
{
    [Authorize] //will restrict controller method to users who meet the authemtication requirement; i.e. CAS
                //[Authorize(Roles = "admin")]
                /*Authorize will check that the user has been authenticated
                                             Roles will check if the user has the admin role
                                             Roles will call GetRolesForUser method in the CASExample2.App_Start.MyRoleProvider class
                                             */
    public class SecretController : Controller
    {
        // GET: Secret
        //[AllowAnonymous]
        public ActionResult Index()
        {
            //ViewBag.Message = "Your application description page.";
            //ICasPrincipal p = System.Web.HttpContext.Current.User;
            ICasPrincipal principal = CasAuthentication.CurrentPrincipal;
            ViewBag.Message = principal.Identity.Name; //System.Security.Principal.GenericIdentity
            return View();
        }
    }
}