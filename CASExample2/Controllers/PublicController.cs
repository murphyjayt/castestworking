﻿using System.Web.Mvc;

namespace CASExample2.Controllers
{
    public class PublicController : Controller
    {
        // GET: Public
        public ActionResult NotAuthorized()
        {
            return View();
        }
    }
}